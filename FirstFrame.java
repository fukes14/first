package TextEngineDesign;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.*;
import java.io.FileReader;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class Frame extends JFrame
{
	private JPanel panel;
	private JTextArea user;
	private JTextArea rules;
	private JMenuBar menuBar;
	//private JButton go;
	private JMenu bar;
	private JMenu menu, submenu;
	private JMenuItem menuItem;
	private JMenuItem go;
	private JRadioButtonMenuItem rbMenuItem;
	private JCheckBoxMenuItem cbMenuItem;

	
	private int width, height;
	private Dimension size;
	
	public Frame(String title, int width, int height)
	{
		this.width = width;
		this.height = height;
		size = new Dimension(width, height);
		this.setTitle(title);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(size);
		this.setLayout(new GridLayout(1, 2, 10, 10));
		this.setResizable(false);
		
		createPanel();
		this.add(panel);
		
	    ScriptEngineManager manager = new ScriptEngineManager();
	    ScriptEngine engine = manager.getEngineByName("js");
	    try 
	    {
	      FileReader reader = new FileReader("D:/HelloWorld.js");
	      engine.eval(reader);
	      reader.close();
	    } 
	    catch (Exception e) 
	    {
	      e.printStackTrace();
	    
	    }
	
	}
	
	private void createPanel()
	{
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(width - 10, height - 10));
		//panel.setLayout(new FlowLayout(FlowLayout.LEADING, 10, 10));
		
		createUser();
		createRules();
		
		panel.add(user);
		
		user.setCaretPosition(user.getText().length());
		
	}//END createPanel
	
	private void createUser()
	{
		//user.getText().endsWith(">")
		
		user = new JTextArea();
		
		//Add carrot
		user.setText(">");
		
		//Keylistener to keep the carrot on the screen
		user.addKeyListener(new KeyListener() {
		    @Override
		    public void keyPressed(KeyEvent e) {
		    	if ((int)e.getKeyChar() == 8)
				{
					if (user.getText().endsWith(">"))
					{
						if (user.getLineCount() == 1)
						{
							user.append(">");
						}
						else
						{
							String temp = user.getText();
							int temp2 = temp.length();
							temp = temp.substring(0, temp2 - 1);
							user.setText(temp);
						}
					}
				}
		    }

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				if ((int)e.getKeyChar() == 10)
				{
					user.append(">");
				}
			}

			@Override
			public void keyTyped(KeyEvent e) 
			{
				
				//System.out.println("typed");
				//System.out.println("linecount: " + user.getLineCount());
				//System.out.println("columbs: " + user.getFocusAccelerator());
				//System.out.println("currant position: " + user.getCaretPosition());
				
			}
		});
		
		user.setPreferredSize(new Dimension(width - 20, height - 40));
		
	}//END createUser
	
	/*
	 * Creates the textarea for the rules of the program
	 */
	private void createRules()
	{
		rules = new JTextArea();
		
		//Set the rules and size of the textarea
		rules.setEditable(false);
		rules.setText("These are the rules of this program and how to make a \ngame in it."
		+ "Type \"Create a room named x.\"\nwhere x is the name you choose.");
		rules.setPreferredSize(new Dimension(400, 400));
		
	}//END createRules
	
	/*
	 * Creates the menubar for the users options
	 */
	private void createMenu()
	{
		menuBar = new JMenuBar();
		
		//Calls method to create the button to compile code
		createGo();
		
		//go.setFocusable(false);
		
		
		
	}//END createMenu
	
	/*
	 * Creates a button that compiles the users text into the code to run the game. 
	 */
	private void createGo()
	{
		go = new JMenuItem("Go");
		
		//Action listener to call method to compile code
		go.addActionListener(new ActionListener()
	    {
	      public void actionPerformed(ActionEvent e)
	      {
	    	System.out.println("Works");
	      }
	    }); //END action listener
		
	}//END createGo
	
}//END Frame